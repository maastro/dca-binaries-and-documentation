# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This readme is meant to give a brief overview of the functionality of the DCA Open Source software repository. This repository has a public issue tracker that can be used to post questions and problems. The figure below shows the components needed to connect to the DLCA-R. Each component will be briefly explained in this document, more details can be found in the technical documentation of the software.

### data mining tools ###
Many care providers already have a solution to aggregate data (Data Warehouse). The DWH can be used as a source to collect the necessary ...[TODO]

### Medical Image Analysis (MIA) ###
Some radiotherapy data is derived from the medical images generated in the care process. To derive this data the Medical Image Analysis framework (MIA) can be used to store this information in a structured manner [https://bitbucket.org/account/user/maastro/projects/MIA]. The framework can calculate any dose volume histogram (DVH) parameter of the delineations created during the clinical process. If necessary, delineations can be combined to get the required parameters. Consult the data dictionary of the DCLA-R to determine what DVH parameters are required. The MIA documentation will provide examples of how to configure calculations.

### data mapping tools ###
The latest version of the data dictionary can be downloaded from the MRDM website [https://mrdm.nl/showcase/downloaden]; this dictionary is needed to map local data to the DCLA-R. 

### external link summary ###
* DCA [https://bitbucket.org/account/user/maastro/projects/DCA]
* MIA [https://bitbucket.org/account/user/maastro/projects/MIA]
* MRDM data dictionaries [https://mrdm.nl/showcase/downloaden]
